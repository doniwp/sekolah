<?php

return [
    'adminEmail' => 'admin@example.com',
    'maskMoneyOptions' => [
        'prefix' => 'Rp ',
        'suffix' => '',
        'affixesStay' => true,
        'thousands' => '.',
        'decimal' => ',',
        'precision' => 2,
        'allowZero' => true,
        'allowNegative' => false,
    ],
];
