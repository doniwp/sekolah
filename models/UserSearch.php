<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User {
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['user_id', 'fk_hak_id'], 'integer'],
            [['user_nik', 'user_nama', 'user_username', 'user_password'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'fk_hak_id' => $this->fk_hak_id,
        ]);

        $query->andFilterWhere(['like', 'user_nik', $this->user_nik])
            ->andFilterWhere(['like', 'user_nama', $this->user_nama])
            ->andFilterWhere(['like', 'user_username', $this->user_username])
            ->andFilterWhere(['like', 'user_password', $this->user_password])
            ->andFilterWhere(['like', 'user_authkey', $this->user_authkey])
            ->andFilterWhere(['like', 'user_password_reset_token', $this->user_password_reset_token]);

        return $dataProvider;
    }
}
