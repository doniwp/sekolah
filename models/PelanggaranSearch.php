<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pelanggaran;

/**
 * PelanggaranSearch represents the model behind the search form about `app\models\Pelanggaran`.
 */
class PelanggaranSearch extends Pelanggaran
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pelanggaran_id', 'pelanggaran_point'], 'integer'],
            [['pelanggaran_kode', 'pelanggaran_nama'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pelanggaran::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pelanggaran_id' => $this->pelanggaran_id,
            'pelanggaran_point' => $this->pelanggaran_point,
        ]);

        $query->andFilterWhere(['like', 'pelanggaran_kode', $this->pelanggaran_kode])
            ->andFilterWhere(['like', 'pelanggaran_nama', $this->pelanggaran_nama]);

        return $dataProvider;
    }
}
