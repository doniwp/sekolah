<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Siswa;

/**
 * SiswaSearch represents the model behind the search form about `app\models\Siswa`.
 */
class SiswaSearch extends Siswa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['siswa_id', 'fk_jurusan_id', 'fk_kelas_id', 'fk_agama_id', 'siswa_point'], 'integer'],
            [['siswa_nis', 'siswa_nama', 'siswa_ortu', 'siswa_alamat', 'siswa_telp', 'siswa_username', 'siswa_password', 'siswa_keterangan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Siswa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'siswa_id' => $this->siswa_id,
            'fk_jurusan_id' => $this->fk_jurusan_id,
            'fk_kelas_id' => $this->fk_kelas_id,
            'fk_agama_id' => $this->fk_agama_id,
            'siswa_point' => $this->siswa_point,
        ]);

        $query->andFilterWhere(['like', 'siswa_nis', $this->siswa_nis])
            ->andFilterWhere(['like', 'siswa_nama', $this->siswa_nama])
            ->andFilterWhere(['like', 'siswa_ortu', $this->siswa_ortu])
            ->andFilterWhere(['like', 'siswa_alamat', $this->siswa_alamat])
            ->andFilterWhere(['like', 'siswa_telp', $this->siswa_telp])
            ->andFilterWhere(['like', 'siswa_username', $this->siswa_username])
            ->andFilterWhere(['like', 'siswa_password', $this->siswa_password])
            ->andFilterWhere(['like', 'siswa_keterangan', $this->siswa_keterangan]);

        return $dataProvider;
    }
}
