<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dst_hak".
 *
 * @property string $hak_id
 * @property string $hak_name
 *
 * @property User[] $dstUsers
 */
class Hak extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'dst_hak';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['hak_name'], 'required'],
            [['hak_name'], 'string', 'max' => 21],
            [['hak_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'hak_id' => 'Hak ID',
            'hak_name' => 'Level Hak Akses',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['fk_hak_id' => 'hak_id']);
    }
}
