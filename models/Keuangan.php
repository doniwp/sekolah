<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dst_keuangan".
 *
 * @property string $keuangan_id
 * @property string $fk_siswa_id
 * @property string $fk_kelas_id
 * @property string $keuangan_keterangan
 * @property double $keuangan_spp
 * @property double $keuangan_total
 * @property double $keuangan_terbayar
 * @property double $keuangan_sisa
 * @property string $keuangan_deadline
 *
 * @property Kelas $fkKelas
 * @property Siswa $fkSiswa
 */
class Keuangan extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'dst_keuangan';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['fk_siswa_id', 'fk_kelas_id', 'keuangan_spp', 'keuangan_total', 'keuangan_terbayar', 'keuangan_sisa', 'keuangan_deadline'], 'required'],
            [['fk_siswa_id', 'fk_kelas_id'], 'integer'],
            [['keuangan_spp', 'keuangan_total', 'keuangan_terbayar', 'keuangan_sisa'], 'number'],
            [['keuangan_deadline'], 'safe'],
            [['keuangan_keterangan'], 'string', 'max' => 50],
            [['fk_kelas_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kelas::className(), 'targetAttribute' => ['fk_kelas_id' => 'kelas_id']],
            [['fk_siswa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Siswa::className(), 'targetAttribute' => ['fk_siswa_id' => 'siswa_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'keuangan_id' => 'Keuangan ID',
            'fk_siswa_id' => 'Nama Siswa',
            'fk_kelas_id' => 'Kelas',
            'keuangan_keterangan' => 'Keterangan',
            'keuangan_spp' => 'Jumlah SPP',
            'keuangan_total' => 'Jumlah Total',
            'keuangan_terbayar' => 'Jumlah Terbayar',
            'keuangan_sisa' => 'Sisa Pembayaran',
            'keuangan_deadline' => 'Deadline',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelas() {
        return $this->hasOne(Kelas::className(), ['kelas_id' => 'fk_kelas_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswa() {
        return $this->hasOne(Siswa::className(), ['siswa_id' => 'fk_siswa_id']);
    }
}
