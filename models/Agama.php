<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dst_agama".
 *
 * @property string $agama_id
 * @property string $agama_name
 *
 * @property Siswa[] $dstSiswas
 */
class Agama extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'dst_agama';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['agama_name'], 'required'],
            [['agama_name'], 'string', 'max' => 21],
            [['agama_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'agama_id' => 'ID Agama',
            'agama_name' => 'Nama Agama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswas() {
        return $this->hasMany(Siswa::className(), ['fk_agama_id' => 'agama_id']);
    }
}
