<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dst_point".
 *
 * @property string $point_id
 * @property string $fk_siswa_id
 * @property string $fk_pelanggaran_id
 * @property string $point_tanggal
 *
 * @property Pelanggaran $fkPelanggaran
 * @property Siswa $fkSiswa
 */
class Point extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'dst_point';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['fk_siswa_id', 'fk_pelanggaran_id', 'point_tanggal'], 'required'],
            [['fk_siswa_id', 'fk_pelanggaran_id'], 'integer'],
            [['point_tanggal'], 'safe'],
            [['fk_pelanggaran_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pelanggaran::className(), 'targetAttribute' => ['fk_pelanggaran_id' => 'pelanggaran_id']],
            [['fk_siswa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Siswa::className(), 'targetAttribute' => ['fk_siswa_id' => 'siswa_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'point_id' => 'Point ID',
            'fk_siswa_id' => 'Nama Siswa',
            'fk_pelanggaran_id' => 'Jenis Pelanggaran',
            'point_tanggal' => 'Tanggal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPelanggaran() {
        return $this->hasOne(Pelanggaran::className(), ['pelanggaran_id' => 'fk_pelanggaran_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswa() {
        return $this->hasOne(Siswa::className(), ['siswa_id' => 'fk_siswa_id']);
    }
}
