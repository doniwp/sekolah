<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dst_absen".
 *
 * @property string $absen_id
 * @property string $fk_siswa_id
 * @property string $fk_kelas_id
 * @property string $fk_jurusan_id
 * @property string $fk_mapel_id
 * @property string $absen_keterangan
 * @property string $absen_tanggal
 *
 * @property Jurusan $fkJurusan
 * @property Kelas $fkKelas
 * @property Mapel $fkMapel
 * @property Siswa $fkSiswa
 */
class Absen extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'dst_absen';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['fk_siswa_id', 'fk_kelas_id', 'fk_jurusan_id', 'fk_mapel_id', 'absen_tanggal'], 'required'],
            [['fk_siswa_id', 'fk_kelas_id', 'fk_jurusan_id', 'fk_mapel_id'], 'integer'],
            [['absen_keterangan'], 'string'],
            [['absen_tanggal'], 'safe'],
            [['fk_jurusan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jurusan::className(), 'targetAttribute' => ['fk_jurusan_id' => 'jurusan_id']],
            [['fk_kelas_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kelas::className(), 'targetAttribute' => ['fk_kelas_id' => 'kelas_id']],
            [['fk_mapel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mapel::className(), 'targetAttribute' => ['fk_mapel_id' => 'mapel_id']],
            [['fk_siswa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Siswa::className(), 'targetAttribute' => ['fk_siswa_id' => 'siswa_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'absen_id' => 'Absen ID',
            'fk_siswa_id' => 'Nama Siswa',
            'fk_kelas_id' => 'Kelas',
            'fk_jurusan_id' => 'Jurusan',
            'fk_mapel_id' => 'Mata Pelajaran',
            'absen_keterangan' => 'Keterangan',
            'absen_tanggal' => 'Tanggal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJurusan() {
        return $this->hasOne(Jurusan::className(), ['jurusan_id' => 'fk_jurusan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelas() {
        return $this->hasOne(Kelas::className(), ['kelas_id' => 'fk_kelas_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapel() {
        return $this->hasOne(Mapel::className(), ['mapel_id' => 'fk_mapel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswa() {
        return $this->hasOne(Siswa::className(), ['siswa_id' => 'fk_siswa_id']);
    }
}
