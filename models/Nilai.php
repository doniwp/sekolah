<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dst_nilai".
 *
 * @property string $nilai_id
 * @property string $fk_kelas_id
 * @property string $fk_mapel_id
 * @property string $fk_siswa_id
 * @property string $nilai_uts
 * @property string $nilai_uas
 * @property string $nilai_ta
 * @property string $fk_jurusan_id
 *
 * @property Jurusan $fkJurusan
 * @property Kelas $fkKelas
 * @property Mapel $fkMapel
 * @property Siswa $fkSiswa
 */
class Nilai extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'dst_nilai';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['fk_kelas_id', 'fk_mapel_id', 'fk_siswa_id', 'nilai_uts', 'nilai_uas', 'nilai_ta', 'fk_jurusan_id'], 'required'],
            [['fk_kelas_id', 'fk_mapel_id', 'fk_siswa_id', 'fk_jurusan_id'], 'integer'],
            [['nilai_uts', 'nilai_uas', 'nilai_ta'], 'number'],
            [['fk_jurusan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jurusan::className(), 'targetAttribute' => ['fk_jurusan_id' => 'jurusan_id']],
            [['fk_kelas_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kelas::className(), 'targetAttribute' => ['fk_kelas_id' => 'kelas_id']],
            [['fk_mapel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mapel::className(), 'targetAttribute' => ['fk_mapel_id' => 'mapel_id']],
            [['fk_siswa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Siswa::className(), 'targetAttribute' => ['fk_siswa_id' => 'siswa_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'nilai_id' => 'Nilai ID',
            'fk_kelas_id' => 'Kelas',
            'fk_mapel_id' => 'Mata Pelajaran',
            'fk_siswa_id' => 'Nama Siswa',
            'nilai_uts' => 'Nilai UTS',
            'nilai_uas' => 'Nilai UAS',
            'nilai_ta' => 'Nilai TA',
            'fk_jurusan_id' => 'Jurusan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJurusan() {
        return $this->hasOne(Jurusan::className(), ['jurusan_id' => 'fk_jurusan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelas() {
        return $this->hasOne(Kelas::className(), ['kelas_id' => 'fk_kelas_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapel() {
        return $this->hasOne(Mapel::className(), ['mapel_id' => 'fk_mapel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswa() {
        return $this->hasOne(Siswa::className(), ['siswa_id' => 'fk_siswa_id']);
    }
}
