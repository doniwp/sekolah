<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Absen;

/**
 * AbsenSearch represents the model behind the search form about `app\models\Absen`.
 */
class AbsenSearch extends Absen
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['absen_id', 'fk_siswa_id', 'fk_kelas_id', 'fk_jurusan_id', 'fk_mapel_id'], 'integer'],
            [['absen_keterangan', 'absen_tanggal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Absen::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'absen_id' => $this->absen_id,
            'fk_siswa_id' => $this->fk_siswa_id,
            'fk_kelas_id' => $this->fk_kelas_id,
            'fk_jurusan_id' => $this->fk_jurusan_id,
            'fk_mapel_id' => $this->fk_mapel_id,
            'absen_tanggal' => $this->absen_tanggal,
        ]);

        $query->andFilterWhere(['like', 'absen_keterangan', $this->absen_keterangan]);

        return $dataProvider;
    }
}
