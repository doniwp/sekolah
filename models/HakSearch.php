<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Hak;

/**
 * HakSearch represents the model behind the search form about `app\models\Hak`.
 */
class HakSearch extends Hak
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hak_id'], 'integer'],
            [['hak_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Hak::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'hak_id' => $this->hak_id,
        ]);

        $query->andFilterWhere(['like', 'hak_name', $this->hak_name]);

        return $dataProvider;
    }
}
