<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Nilai;

/**
 * NilaiSearch represents the model behind the search form about `app\models\Nilai`.
 */
class NilaiSearch extends Nilai
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nilai_id', 'fk_kelas_id', 'fk_mapel_id', 'fk_siswa_id', 'fk_jurusan_id'], 'integer'],
            [['nilai_uts', 'nilai_uas', 'nilai_ta'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Nilai::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'nilai_id' => $this->nilai_id,
            'fk_kelas_id' => $this->fk_kelas_id,
            'fk_mapel_id' => $this->fk_mapel_id,
            'fk_siswa_id' => $this->fk_siswa_id,
            'nilai_uts' => $this->nilai_uts,
            'nilai_uas' => $this->nilai_uas,
            'nilai_ta' => $this->nilai_ta,
            'fk_jurusan_id' => $this->fk_jurusan_id,
        ]);

        return $dataProvider;
    }
}
