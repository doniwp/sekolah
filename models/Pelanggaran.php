<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dst_pelanggaran".
 *
 * @property string $pelanggaran_id
 * @property string $pelanggaran_kode
 * @property string $pelanggaran_nama
 * @property integer $pelanggaran_point
 *
 * @property Point[] $dstPoints
 */
class Pelanggaran extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'dst_pelanggaran';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['pelanggaran_kode', 'pelanggaran_nama', 'pelanggaran_point'], 'required'],
            [['pelanggaran_point'], 'integer'],
            [['pelanggaran_kode'], 'string', 'max' => 21],
            [['pelanggaran_nama'], 'string', 'max' => 51],
            [['pelanggaran_kode', 'pelanggaran_nama', 'pelanggaran_point'], 'unique', 'targetAttribute' => ['pelanggaran_kode', 'pelanggaran_nama', 'pelanggaran_point'], 'message' => 'The combination of Pelanggaran Kode, Pelanggaran Nama and Pelanggaran Point has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'pelanggaran_id' => 'Pelanggaran ID',
            'pelanggaran_kode' => 'Kode Pelanggaran',
            'pelanggaran_nama' => 'Nama Pelanggaran',
            'pelanggaran_point' => 'Poin Pelanggaran',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoints() {
        return $this->hasMany(Point::className(), ['fk_pelanggaran_id' => 'pelanggaran_id']);
    }
}
