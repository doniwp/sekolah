<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Point;

/**
 * PointSearch represents the model behind the search form about `app\models\Point`.
 */
class PointSearch extends Point
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_id', 'fk_siswa_id', 'fk_pelanggaran_id'], 'integer'],
            [['point_tanggal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Point::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'point_id' => $this->point_id,
            'fk_siswa_id' => $this->fk_siswa_id,
            'fk_pelanggaran_id' => $this->fk_pelanggaran_id,
            'point_tanggal' => $this->point_tanggal,
        ]);

        return $dataProvider;
    }
}
