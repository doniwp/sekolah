<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dst_mapel".
 *
 * @property string $mapel_id
 * @property string $mapel_kode
 * @property string $mapel_nama
 * @property string $fk_user_id
 *
 * @property Absen[] $dstAbsens
 * @property User $fkUser
 * @property Nilai[] $dstNilais
 */
class Mapel extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'dst_mapel';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['mapel_kode', 'mapel_nama', 'fk_user_id'], 'required'],
            [['fk_user_id'], 'integer'],
            [['mapel_kode', 'mapel_nama'], 'string', 'max' => 21],
            [['mapel_kode', 'mapel_nama'], 'unique', 'targetAttribute' => ['mapel_kode', 'mapel_nama'], 'message' => 'The combination of Mapel Kode and Mapel Nama has already been taken.'],
            [['fk_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'mapel_id' => 'Mapel ID',
            'mapel_kode' => 'Kode Mata Pelajaran',
            'mapel_nama' => 'Nama Mata Pelajaran',
            'fk_user_id' => 'Nama Guru',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbsens() {
        return $this->hasMany(Absen::className(), ['fk_mapel_id' => 'mapel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['user_id' => 'fk_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNilais() {
        return $this->hasMany(Nilai::className(), ['fk_mapel_id' => 'mapel_id']);
    }
}
