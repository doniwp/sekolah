<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dst_kelas".
 *
 * @property string $kelas_id
 * @property string $kelas_kode
 * @property integer $kelas_grade
 * @property integer $kelas_semester
 * @property string $fk_user_id
 *
 * @property Absen[] $dstAbsens
 * @property User $fkUser
 * @property Keuangan[] $dstKeuangans
 * @property Nilai[] $dstNilais
 * @property Siswa[] $dstSiswas
 */
class Kelas extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'dst_kelas';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['kelas_kode', 'kelas_grade', 'kelas_semester', 'fk_user_id'], 'required'],
            [['kelas_grade', 'kelas_semester', 'fk_user_id'], 'integer'],
            [['kelas_kode'], 'string', 'max' => 21],
            [['kelas_kode', 'kelas_grade', 'kelas_semester'], 'unique', 'targetAttribute' => ['kelas_kode', 'kelas_grade', 'kelas_semester'], 'message' => 'The combination of Kelas Kode, Kelas Grade and Kelas Semester has already been taken.'],
            [['fk_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'kelas_id' => 'Kelas ID',
            'kelas_kode' => 'Kode Kelas',
            'kelas_grade' => 'Grade Kelas',
            'kelas_semester' => 'Semester',
            'fk_user_id' => 'Wali Kelas',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbsens() {
        return $this->hasMany(Absen::className(), ['fk_kelas_id' => 'kelas_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['user_id' => 'fk_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeuangans() {
        return $this->hasMany(Keuangan::className(), ['fk_kelas_id' => 'kelas_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNilais() {
        return $this->hasMany(Nilai::className(), ['fk_kelas_id' => 'kelas_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswas() {
        return $this->hasMany(Siswa::className(), ['fk_kelas_id' => 'kelas_id']);
    }
}
