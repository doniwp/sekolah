<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Keuangan;

/**
 * KeuanganSearch represents the model behind the search form about `app\models\Keuangan`.
 */
class KeuanganSearch extends Keuangan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keuangan_id', 'fk_siswa_id', 'fk_kelas_id'], 'integer'],
            [['keuangan_keterangan', 'keuangan_deadline'], 'safe'],
            [['keuangan_spp', 'keuangan_total', 'keuangan_terbayar', 'keuangan_sisa'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Keuangan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'keuangan_id' => $this->keuangan_id,
            'fk_siswa_id' => $this->fk_siswa_id,
            'fk_kelas_id' => $this->fk_kelas_id,
            'keuangan_spp' => $this->keuangan_spp,
            'keuangan_total' => $this->keuangan_total,
            'keuangan_terbayar' => $this->keuangan_terbayar,
            'keuangan_sisa' => $this->keuangan_sisa,
            'keuangan_deadline' => $this->keuangan_deadline,
        ]);

        $query->andFilterWhere(['like', 'keuangan_keterangan', $this->keuangan_keterangan]);

        return $dataProvider;
    }
}
