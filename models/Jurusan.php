<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dst_jurusan".
 *
 * @property string $jurusan_id
 * @property string $jurusan_kode
 * @property string $jurusan_nama
 *
 * @property Absen[] $dstAbsens
 * @property Nilai[] $dstNilais
 * @property Siswa[] $dstSiswas
 */
class Jurusan extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'dst_jurusan';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['jurusan_kode', 'jurusan_nama'], 'required'],
            [['jurusan_kode'], 'string', 'max' => 21],
            [['jurusan_nama'], 'string', 'max' => 51],
            [['jurusan_kode', 'jurusan_nama'], 'unique', 'targetAttribute' => ['jurusan_kode', 'jurusan_nama'], 'message' => 'The combination of Jurusan Kode and Jurusan Nama has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'jurusan_id' => 'Jurusan ID',
            'jurusan_kode' => 'Kode Jurusan',
            'jurusan_nama' => 'Nama Jurusan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbsens() {
        return $this->hasMany(Absen::className(), ['fk_jurusan_id' => 'jurusan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNilais() {
        return $this->hasMany(Nilai::className(), ['fk_jurusan_id' => 'jurusan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswas() {
        return $this->hasMany(Siswa::className(), ['fk_jurusan_id' => 'jurusan_id']);
    }
}
