<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Mapel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mapel-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mapel_kode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mapel_nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fk_user_id')->dropDownList(ArrayHelper::map(User::find()->all(), 'user_id', 'user_nama'), ['prompt' => 'Select Guru']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
