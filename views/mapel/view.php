<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mapel */

$this->title = $model->mapel_id;
$this->params['breadcrumbs'][] = ['label' => 'Mata Pelajaran', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mapel-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->mapel_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->mapel_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'mapel_id',
            'mapel_kode',
            'mapel_nama',
            [
                'attribute' => 'fk_user_id',
                'value' => $model->user->user_nama,
            ],
        ],
    ]) ?>

</div>
