<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MapelSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mapel-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'mapel_id') ?>

    <?= $form->field($model, 'mapel_kode') ?>

    <?= $form->field($model, 'mapel_nama') ?>

    <?= $form->field($model, 'fk_user_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
