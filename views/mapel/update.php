<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mapel */

$this->title = 'Update : ' . $model->mapel_id;
$this->params['breadcrumbs'][] = ['label' => 'Mata Pelajaran', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->mapel_id, 'url' => ['view', 'id' => $model->mapel_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mapel-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
