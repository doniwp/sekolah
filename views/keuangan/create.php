<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Keuangan */

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => 'Keuangan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="keuangan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
