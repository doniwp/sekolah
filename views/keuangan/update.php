<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Keuangan */

$this->title = 'Update : ' . $model->keuangan_id;
$this->params['breadcrumbs'][] = ['label' => 'Keuangan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->keuangan_id, 'url' => ['view', 'id' => $model->keuangan_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="keuangan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
