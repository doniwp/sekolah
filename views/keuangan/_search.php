<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KeuanganSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="keuangan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'keuangan_id') ?>

    <?= $form->field($model, 'fk_siswa_id') ?>

    <?= $form->field($model, 'fk_kelas_id') ?>

    <?= $form->field($model, 'keuangan_keterangan') ?>

    <?= $form->field($model, 'keuangan_spp') ?>

    <?php // echo $form->field($model, 'keuangan_total') ?>

    <?php // echo $form->field($model, 'keuangan_terbayar') ?>

    <?php // echo $form->field($model, 'keuangan_sisa') ?>

    <?php // echo $form->field($model, 'keuangan_deadline') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
