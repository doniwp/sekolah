<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KeuanganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Keuangan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="keuangan-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Yii::$app->user->identity->fk_hak_id != 2 ? Html::a('Create', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'keuangan_id',
            [
                'attribute' => 'fk_siswa_id',
                'value' => 'siswa.siswa_nama',
            ],
            [
                'attribute' => 'fk_kelas_id',
                'value' => 'kelas.kelas_kode',
            ],
//            'keuangan_keterangan',
//            'keuangan_spp',
            [
                'attribute' => 'keuangan_total',
                'format' => 'currency',
            ],
            [
                'attribute' => 'keuangan_terbayar',
                'format' => 'currency',
            ],
            [
                'attribute' => 'keuangan_sisa',
                'format' => 'currency',
            ],
            'keuangan_deadline',

            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => function ($model, $key, $index) {
                        return Yii::$app->user->identity->fk_hak_id != 2 ? true : false;
                    },
                    'delete' => function ($model, $key, $index) {
                        return Yii::$app->user->identity->fk_hak_id != 2 ? true : false;
                    }
                ]
            ],
        ],
    ]); ?>
</div>
