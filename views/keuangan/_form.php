<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use kartik\money\MaskMoney;
use app\models\Siswa;
use app\models\Kelas;

/* @var $this yii\web\View */
/* @var $model app\models\Keuangan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="keuangan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fk_siswa_id')->dropDownList(ArrayHelper::map(Siswa::find()->all(), 'siswa_id', 'siswa_nama'), ['prompt' => 'Select Siswa']) ?>

    <?= $form->field($model, 'fk_kelas_id')->dropDownList(ArrayHelper::map(Kelas::find()->all(), 'kelas_id', 'kelas_kode'), ['prompt' => 'Select Kelas']) ?>

    <?= $form->field($model, 'keuangan_keterangan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keuangan_spp')->widget(MaskMoney::className(), [
        'pluginOptions' => [
            'prefix' => 'Rp ',
            'suffix' => '',
            'allowNegative' => false,
            'precision' => 0
        ]
    ]) ?>

    <?= $form->field($model, 'keuangan_total')->widget(MaskMoney::className(), [
        'pluginOptions' => [
            'prefix' => 'Rp ',
            'suffix' => '',
            'allowNegative' => false,
            'precision' => 0
        ]
    ]) ?>

    <?= $form->field($model, 'keuangan_terbayar')->widget(MaskMoney::className(), [
        'pluginOptions' => [
            'prefix' => 'Rp ',
            'suffix' => '',
            'allowNegative' => false,
            'precision' => 0
        ]
    ]) ?>

    <?= $form->field($model, 'keuangan_sisa')->widget(MaskMoney::className(), [
        'pluginOptions' => [
            'prefix' => 'Rp ',
            'suffix' => '',
            'allowNegative' => false,
            'precision' => 0
        ]
    ]) ?>

    <?= $form->field($model, 'keuangan_deadline')->widget(DatePicker::className(), [
        'dateFormat' => 'yyyy-MM-dd',
        'clientOptions' => [
            'changeMonth' => true,
            'changeYear' => true,
        ],
        'options' => [
            'class' => 'form-control',
            'readOnly' => 'readOnly',
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
