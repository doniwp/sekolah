<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Keuangan */

$this->title = $model->keuangan_id;
$this->params['breadcrumbs'][] = ['label' => 'Keuangan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="keuangan-view">

    <p>
        <?= Yii::$app->user->identity->fk_hak_id != 2 ? Html::a('Update', ['update', 'id' => $model->keuangan_id], ['class' => 'btn btn-primary']) : '' ?>
        <?= Yii::$app->user->identity->fk_hak_id != 2 ? Html::a('Delete', ['delete', 'id' => $model->keuangan_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'keuangan_id',
            [
                'attribute' => 'fk_siswa_id',
                'value' => $model->siswa->siswa_nama,
            ],
            [
                'attribute' => 'fk_kelas_id',
                'value' => $model->kelas->kelas_kode,
            ],
            'keuangan_keterangan',
            [
                'attribute' => 'keuangan_spp',
                'format' => 'currency',
            ],
            [
                'attribute' => 'keuangan_total',
                'format' => 'currency',
            ],
            [
                'attribute' => 'keuangan_terbayar',
                'format' => 'currency',
            ],
            [
                'attribute' => 'keuangan_sisa',
                'format' => 'currency',
            ],
            'keuangan_deadline',
        ],
    ]) ?>

</div>
