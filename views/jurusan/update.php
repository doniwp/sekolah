<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Jurusan */

$this->title = 'Update : ' . $model->jurusan_id;
$this->params['breadcrumbs'][] = ['label' => 'Jurusan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->jurusan_id, 'url' => ['view', 'id' => $model->jurusan_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jurusan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
