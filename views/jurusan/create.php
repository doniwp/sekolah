<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Jurusan */

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => 'Jurusan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jurusan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
