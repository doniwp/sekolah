<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Agama */

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => 'Agama', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agama-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
