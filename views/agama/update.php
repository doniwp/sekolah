<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Agama */

$this->title = 'Update : ' . $model->agama_id;
$this->params['breadcrumbs'][] = ['label' => 'Agama', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->agama_id, 'url' => ['view', 'id' => $model->agama_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="agama-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
