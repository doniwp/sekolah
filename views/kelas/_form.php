<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Kelas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kelas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kelas_kode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kelas_grade')->textInput() ?>

    <?= $form->field($model, 'kelas_semester')->textInput() ?>

    <?= $form->field($model, 'fk_user_id')->dropDownList(ArrayHelper::map(User::find()->all(), 'user_id', 'user_nama'), ['prompt' => 'Select Wali Kelas']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
