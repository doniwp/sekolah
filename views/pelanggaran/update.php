<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pelanggaran */

$this->title = 'Update : ' . $model->pelanggaran_id;
$this->params['breadcrumbs'][] = ['label' => 'Jenis Pelanggaran', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pelanggaran_id, 'url' => ['view', 'id' => $model->pelanggaran_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pelanggaran-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
