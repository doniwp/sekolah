<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pelanggaran */

$this->title = $model->pelanggaran_id;
$this->params['breadcrumbs'][] = ['label' => 'Jenis Pelanggaran', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pelanggaran-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->pelanggaran_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->pelanggaran_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'pelanggaran_id',
            'pelanggaran_kode',
            'pelanggaran_nama',
            'pelanggaran_point',
        ],
    ]) ?>

</div>
