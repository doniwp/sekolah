<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pelanggaran */

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => 'Jenis Pelanggaran', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pelanggaran-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
