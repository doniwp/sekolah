<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PelanggaranSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jenis Pelanggaran';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pelanggaran-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'pelanggaran_id',
            'pelanggaran_kode',
            'pelanggaran_nama',
            'pelanggaran_point',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
