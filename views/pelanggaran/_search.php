<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PelanggaranSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pelanggaran-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'pelanggaran_id') ?>

    <?= $form->field($model, 'pelanggaran_kode') ?>

    <?= $form->field($model, 'pelanggaran_nama') ?>

    <?= $form->field($model, 'pelanggaran_point') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
