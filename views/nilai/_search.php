<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NilaiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nilai-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'nilai_id') ?>

    <?= $form->field($model, 'fk_kelas_id') ?>

    <?= $form->field($model, 'fk_mapel_id') ?>

    <?= $form->field($model, 'fk_siswa_id') ?>

    <?= $form->field($model, 'nilai_uts') ?>

    <?php // echo $form->field($model, 'nilai_uas') ?>

    <?php // echo $form->field($model, 'nilai_ta') ?>

    <?php // echo $form->field($model, 'fk_jurusan_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
