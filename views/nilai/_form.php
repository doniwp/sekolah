<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\MaskedInput;
use app\models\Siswa;
use app\models\Kelas;
use app\models\Jurusan;
use app\models\Mapel;

/* @var $this yii\web\View */
/* @var $model app\models\Nilai */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nilai-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fk_kelas_id')->dropDownList(ArrayHelper::map(Kelas::find()->all(), 'kelas_id', 'kelas_kode'), ['prompt' => 'Select Kelas']) ?>

    <?= $form->field($model, 'fk_mapel_id')->dropDownList(ArrayHelper::map(Mapel::find()->all(), 'mapel_id', 'mapel_nama'), ['prompt' => 'Select Mata Pelajaran']) ?>

    <?= $form->field($model, 'fk_siswa_id')->dropDownList(ArrayHelper::map(Siswa::find()->all(), 'siswa_id', 'siswa_nama'), ['prompt' => 'Select Siswa']) ?>

    <?= $form->field($model, 'nilai_uts')->widget(MaskedInput::className(), [
        'name' => 'input-33',
        'clientOptions' => [
            'alias' => 'decimal',
            'groupSeparator' => '.',
            'radixPoint' => ',',
            'autoGroup' => true
        ],
    ]) ?>

    <?= $form->field($model, 'nilai_uas')->widget(MaskedInput::className(), [
        'name' => 'input-33',
        'clientOptions' => [
            'alias' => 'decimal',
            'groupSeparator' => '.',
            'radixPoint' => ',',
            'autoGroup' => true
        ],
    ]) ?>

    <?= $form->field($model, 'nilai_ta')->widget(MaskedInput::className(), [
        'name' => 'input-33',
        'clientOptions' => [
            'alias' => 'decimal',
            'groupSeparator' => '.',
            'radixPoint' => ',',
            'autoGroup' => true
        ],
    ]) ?>

    <?= $form->field($model, 'fk_jurusan_id')->dropDownList(ArrayHelper::map(Jurusan::find()->all(), 'jurusan_id', 'jurusan_nama'), ['prompt' => 'Select Jurusan']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
