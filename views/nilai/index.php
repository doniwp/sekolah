<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NilaiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nilai';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nilai-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Yii::$app->user->identity->fk_hak_id != 2 ? Html::a('Create', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'nilai_id',
            [
                'attribute' => 'fk_kelas_id',
                'value' => 'kelas.kelas_kode',
            ],
            [
                'attribute' => 'fk_mapel_id',
                'value' => 'mapel.mapel_nama',
            ],
            [
                'attribute' => 'fk_siswa_id',
                'value' => 'siswa.siswa_nama',
            ],
            'nilai_uts',
            'nilai_uas',
            'nilai_ta',
            [
                'attribute' => 'fk_jurusan_id',
                'value' => 'jurusan.jurusan_nama',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => function ($model, $key, $index) {
                        return Yii::$app->user->identity->fk_hak_id != 2 ? true : false;
                    },
                    'delete' => function ($model, $key, $index) {
                        return Yii::$app->user->identity->fk_hak_id != 2 ? true : false;
                    }
                ]
            ],
        ],
    ]); ?>
</div>
