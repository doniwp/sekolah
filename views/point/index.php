<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PointSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pelanggaran';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="point-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Yii::$app->user->identity->fk_hak_id != 2 ? Html::a('Create', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'point_id',
            [
                'attribute' => 'fk_siswa_id',
                'value' => 'siswa.siswa_nama',
            ],
            [
                'attribute' => 'fk_pelanggaran_id',
                'value' => 'pelanggaran.pelanggaran_nama',
            ],
            [
                'attribute' => 'fk_pelanggaran_id',
                'label' => 'Point',
                'value' => 'pelanggaran.pelanggaran_point',
            ],
            'point_tanggal',

            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => function ($model, $key, $index) {
                        return Yii::$app->user->identity->fk_hak_id != 2 ? true : false;
                    },
                    'delete' => function ($model, $key, $index) {
                        return Yii::$app->user->identity->fk_hak_id != 2 ? true : false;
                    }
                ]
            ],
        ],
    ]); ?>
</div>
