<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Point */

$this->title = 'Update : ' . $model->point_id;
$this->params['breadcrumbs'][] = ['label' => 'Pelanggaran', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->point_id, 'url' => ['view', 'id' => $model->point_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="point-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
