<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use app\models\Siswa;
use app\models\Pelanggaran;

/* @var $this yii\web\View */
/* @var $model app\models\Point */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="point-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fk_siswa_id')->dropDownList(ArrayHelper::map(Siswa::find()->all(), 'siswa_id', 'siswa_nama'), ['prompt' => 'Select Siswa']) ?>

    <?= $form->field($model, 'fk_pelanggaran_id')->dropDownList(ArrayHelper::map(Pelanggaran::find()->all(), 'pelanggaran_id', 'pelanggaran_nama'), ['prompt' => 'Select Pelanggaran']) ?>

    <?= $form->field($model, 'point_tanggal')->widget(DatePicker::className(), [
        'dateFormat' => 'yyyy-MM-dd',
        'clientOptions' => [
            'changeMonth' => true,
            'changeYear' => true,
        ],
        'options' => [
            'class' => 'form-control',
            'readOnly' => 'readOnly',
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
