<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Point */

$this->title = $model->point_id;
$this->params['breadcrumbs'][] = ['label' => 'Pelanggaran', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="point-view">

    <p>
        <?= Yii::$app->user->identity->fk_hak_id != 2 ? Html::a('Update', ['update', 'id' => $model->point_id], ['class' => 'btn btn-primary']) : '' ?>
        <?= Yii::$app->user->identity->fk_hak_id != 2 ? Html::a('Delete', ['delete', 'id' => $model->point_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'point_id',
            [
                'attribute' => 'fk_siswa_id',
                'value' => $model->siswa->siswa_nama,
            ],
            [
                'attribute' => 'fk_pelanggaran_id',
                'value' => $model->pelanggaran->pelanggaran_nama,
            ],
            [
                'attribute' => 'fk_pelanggaran_id',
                'label' => 'Point',
                'value' => $model->pelanggaran->pelanggaran_point,
            ],
            'point_tanggal',
        ],
    ]) ?>

</div>
