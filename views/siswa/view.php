<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Siswa */

$this->title = $model->siswa_id;
$this->params['breadcrumbs'][] = ['label' => 'Siswa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="siswa-view">

    <p>
        <?= Yii::$app->user->identity->fk_hak_id != 3 && Yii::$app->user->identity->fk_hak_id != 4 && Yii::$app->user->identity->fk_hak_id != 5 ? Html::a('Update', ['update', 'id' => $model->siswa_id], ['class' => 'btn btn-primary']) : '' ?>
        <?= Yii::$app->user->identity->fk_hak_id != 2 && Yii::$app->user->identity->fk_hak_id != 3 && Yii::$app->user->identity->fk_hak_id != 4 && Yii::$app->user->identity->fk_hak_id != 5 ? Html::a('Delete', ['delete', 'id' => $model->siswa_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'siswa_id',
            'siswa_nis',
            'siswa_nama',
            [
                'attribute' => 'fk_jurusan_id',
                'value' => $model->jurusan->jurusan_nama,
            ],
            [
                'attribute' => 'fk_kelas_id',
                'value' => $model->kelas->kelas_kode,
            ],
            [
                'attribute' => 'fk_agama_id',
                'value' => $model->agama->agama_name,
            ],
            'siswa_ortu',
            'siswa_alamat:ntext',
            'siswa_telp',
            'siswa_username',
//            'siswa_password',
            'siswa_point',
            'siswa_keterangan',
        ],
    ]) ?>

</div>
