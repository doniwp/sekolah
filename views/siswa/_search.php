<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SiswaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="siswa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'siswa_id') ?>

    <?= $form->field($model, 'siswa_nis') ?>

    <?= $form->field($model, 'siswa_nama') ?>

    <?= $form->field($model, 'fk_jurusan_id') ?>

    <?= $form->field($model, 'fk_kelas_id') ?>

    <?php // echo $form->field($model, 'fk_agama_id') ?>

    <?php // echo $form->field($model, 'siswa_ortu') ?>

    <?php // echo $form->field($model, 'siswa_alamat') ?>

    <?php // echo $form->field($model, 'siswa_telp') ?>

    <?php // echo $form->field($model, 'siswa_username') ?>

    <?php // echo $form->field($model, 'siswa_password') ?>

    <?php // echo $form->field($model, 'siswa_point') ?>

    <?php // echo $form->field($model, 'siswa_keterangan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
