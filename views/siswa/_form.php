<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Jurusan;
use app\models\Kelas;
use app\models\Agama;

/* @var $this yii\web\View */
/* @var $model app\models\Siswa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="siswa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'siswa_nis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'siswa_nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fk_jurusan_id')->dropDownList(ArrayHelper::map(Jurusan::find()->all(), 'jurusan_id', 'jurusan_nama'), ['prompt' => 'Select Jurusan']) ?>

    <?= $form->field($model, 'fk_kelas_id')->dropDownList(ArrayHelper::map(Kelas::find()->all(), 'kelas_id', 'kelas_kode'), ['prompt' => 'Select Kelas']) ?>

    <?= $form->field($model, 'fk_agama_id')->dropDownList(ArrayHelper::map(Agama::find()->all(), 'agama_id', 'agama_name'), ['prompt' => 'Select Agama']) ?>

    <?= $form->field($model, 'siswa_ortu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'siswa_alamat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'siswa_telp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'siswa_username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'siswa_password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'siswa_keterangan')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
