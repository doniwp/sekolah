<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Siswa';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="siswa-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Yii::$app->user->identity->fk_hak_id != 2 && Yii::$app->user->identity->fk_hak_id != 3 && Yii::$app->user->identity->fk_hak_id != 4 && Yii::$app->user->identity->fk_hak_id != 5 ? Html::a('Create', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'siswa_id',
            'siswa_nis',
            'siswa_nama',
            [
                'attribute' => 'fk_jurusan_id',
                'value' => 'jurusan.jurusan_nama',
            ],
            [
                'attribute' => 'fk_kelas_id',
                'value' => 'kelas.kelas_kode',
            ],
            // 'fk_agama_id',
            'siswa_ortu',
            // 'siswa_alamat:ntext',
            'siswa_telp',
            // 'siswa_username',
            // 'siswa_password',
            // 'siswa_point',
            // 'siswa_keterangan',

            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => function ($model, $key, $index) {
                        return Yii::$app->user->identity->fk_hak_id != 3 && Yii::$app->user->identity->fk_hak_id != 4 && Yii::$app->user->identity->fk_hak_id != 5 ? true : false;
                    },
                    'delete' => function ($model, $key, $index) {
                        return Yii::$app->user->identity->fk_hak_id != 2 && Yii::$app->user->identity->fk_hak_id != 3 && Yii::$app->user->identity->fk_hak_id != 4 && Yii::$app->user->identity->fk_hak_id != 5 ? true : false;
                    }
                ]
            ],
        ],
    ]); ?>
</div>
