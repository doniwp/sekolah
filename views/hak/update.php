<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Hak */

$this->title = 'Update : ' . $model->hak_id;
$this->params['breadcrumbs'][] = ['label' => 'Level Hak Akses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->hak_id, 'url' => ['view', 'id' => $model->hak_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hak-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
