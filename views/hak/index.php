<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HakSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Level Hak Akses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hak-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'hak_id',
            'hak_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
