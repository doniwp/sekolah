<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Absen */

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => 'Absensi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="absen-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
