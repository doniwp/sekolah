<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Absen */

$this->title = $model->absen_id;
$this->params['breadcrumbs'][] = ['label' => 'Absensi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="absen-view">

    <p>
        <?= Yii::$app->user->identity->fk_hak_id != 2 ? Html::a('Update', ['update', 'id' => $model->absen_id], ['class' => 'btn btn-primary']) : '' ?>
        <?= Yii::$app->user->identity->fk_hak_id != 2 ? Html::a('Delete', ['delete', 'id' => $model->absen_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'absen_id',
            [
                'attribute' => 'fk_siswa_id',
                'value' => $model->siswa->siswa_nama,
            ],
            [
                'attribute' => 'fk_kelas_id',
                'value' => $model->kelas->kelas_kode,
            ],
            [
                'attribute' => 'fk_jurusan_id',
                'value' => $model->jurusan->jurusan_nama,
            ],
            [
                'attribute' => 'fk_mapel_id',
                'value' => $model->mapel->mapel_nama,
            ],
            'absen_keterangan',
            'absen_tanggal',
        ],
    ]) ?>

</div>
