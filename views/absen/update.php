<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Absen */

$this->title = 'Update : ' . $model->absen_id;
$this->params['breadcrumbs'][] = ['label' => 'Absensi', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->absen_id, 'url' => ['view', 'id' => $model->absen_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="absen-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
